package id.afif.binarchallenge8

import android.app.Application
import id.afif.binarchallenge8.network.ApiClient
import id.afif.binarchallenge8.network.model.ResultDtoMapper
import id.afif.binarchallenge8.presentation.ui.MoviesViewModel
import id.afif.binarchallenge8.repository.PopularMoviesRepoImpl
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@App)
            modules(listOf(appModule))
        }
    }

    private val appModule = module {
        single { ApiClient.instance }
        single { ResultDtoMapper() }
        single { PopularMoviesRepoImpl(get(), get()) }
        single { MoviesViewModel(get()) }
    }
}