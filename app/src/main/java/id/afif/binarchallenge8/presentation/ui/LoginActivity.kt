package id.afif.binarchallenge8.presentation.ui

import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.blur
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import id.afif.binarchallenge8.R

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            BackgroundTesting()
        }
    }

    @Composable
    @Preview(showSystemUi = true)
    fun BackgroundTesting() {
        val hitam = colorResource(id = R.color.black)
        val darkBlue = colorResource(id = R.color.dark_blue)

        val gradientBlue = Brush.verticalGradient(0f to darkBlue, 1000f to hitam)

        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.TopCenter
        ) {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(gradientBlue)
                    .blur(16.dp)
            )
            Login()
        }

    }

    @Composable
    @Preview(showSystemUi = false)
    fun Login() {
        var username by remember { mutableStateOf("") }
        var password by remember { mutableStateOf("") }
        var showPassword by remember { mutableStateOf(false) }
        Column(
            modifier = Modifier.padding(20.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = "LOGIN",
                fontSize = 36.sp,
                fontWeight = FontWeight.Bold,
                color = Color.White,
                textAlign = TextAlign.Center,
                modifier = Modifier.padding(top = 36.dp)
            )
            Spacer(
                modifier = Modifier.padding(25.dp)
            )

            CustomTextField(
                text = username,
                onValueChange = {
                    username = it
                },
                label = "Username",
                leftIcon = {
                    Icon(
                        painterResource(id = R.drawable.user),
                        contentDescription = "",
                        modifier = Modifier.size(20.dp),
                        tint = Color.White
                    )
                },
                visualTransformation = VisualTransformation.None

            )


            Spacer(modifier = Modifier.padding(5.dp))

            CustomTextField(
                text = password,
                onValueChange = {
                    password = it
                },
                label = "Password",
                leftIcon = {
                    Icon(
                        painter = painterResource(id = R.drawable.key),
                        contentDescription = "",
                        modifier = Modifier.size(20.dp),
                        tint = Color.White
                    )
                },
                rightIcon = {
                    if (showPassword) {
                        IconButton(onClick = { showPassword = false }) {
                            Icon(
                                painterResource(id = R.drawable.ic_outline_visibility_24),
                                modifier = Modifier.size(20.dp),
                                contentDescription = "",
                                tint = colorResource(id = R.color.light_blue)
                            )
                        }

                    } else {
                        IconButton(onClick = { showPassword = true }) {
                            Icon(
                                painterResource(id = R.drawable.ic_baseline_visibility_off_24),
                                contentDescription = "",
                                tint = colorResource(id = R.color.light_blue),
                                modifier = Modifier.size(20.dp)
                            )
                        }
                    }
                },
                visualTransformation = if (showPassword) VisualTransformation.None else PasswordVisualTransformation()


            )


            Spacer(modifier = Modifier.padding(5.dp))
            Text(
                text = "Forget Password?",
                color = Color.White,
                modifier = Modifier.fillMaxWidth(0.8f),
                textAlign = TextAlign.Right
            )

            Spacer(modifier = Modifier.padding(10.dp))

            Button(
                onClick = {
                    val intent = Intent(applicationContext, HomeActivity::class.java)
                    startActivity(intent)
                },
                modifier = Modifier
                    .fillMaxWidth(0.8f),
                shape = RoundedCornerShape(5.dp),
                colors = ButtonDefaults.buttonColors(backgroundColor = colorResource(id = R.color.dark_blue))
            ) {
                Text(text = "LOGIN", color = Color.White)
            }

            Spacer(modifier = Modifier.padding(10.dp))

            Text(
                text = "Don't have account?",
                fontWeight = FontWeight.Bold,
                color = Color.White,
                modifier = Modifier.clickable {
                    val intent = Intent(applicationContext, RegisterActivity::class.java)
                    startActivity(intent)
                }
            )

            Spacer(modifier = Modifier.padding(10.dp))

            Divider(
                color = Color.LightGray,
                thickness = 1.dp,
                modifier = Modifier.fillMaxWidth(0.8f)
            )

            Spacer(modifier = Modifier.padding(10.dp))

            Button(
                onClick = { /*TODO*/ },
                colors = ButtonDefaults.buttonColors(backgroundColor = Color.Black),
                modifier = Modifier.fillMaxWidth(0.8f)
            ) {
                Image(
                    painter = painterResource(id = R.drawable.google_icon),
                    contentDescription = "",
                    contentScale = ContentScale.Crop,
                    modifier = Modifier.size(20.dp)
                )
                Text(text = "\tSign in With Google", color = Color.White)
            }

        }
    }

    @Composable
    fun CustomTextField(
        text: String,
        leftIcon: @Composable (() -> Unit)? = null,
        rightIcon: @Composable (() -> Unit)? = null,
        onValueChange: (String) -> Unit,
        label: String,
        visualTransformation: VisualTransformation

    ) {
        OutlinedTextField(
            value = text,
            modifier = Modifier.fillMaxWidth(0.8f),
            onValueChange = onValueChange,
            shape = RoundedCornerShape(10.dp),
            leadingIcon = leftIcon,
            trailingIcon = rightIcon,
            label = { Text(text = label, color = Color.Gray) },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = Color.LightGray,
                unfocusedBorderColor = Color.Gray,
                textColor = Color.White, cursorColor = colorResource(id = R.color.light_blue)
            ),
            visualTransformation = visualTransformation
        )
    }


}