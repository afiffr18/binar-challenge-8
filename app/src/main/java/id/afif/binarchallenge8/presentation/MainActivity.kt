package id.afif.binarchallenge8.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import id.afif.binarchallenge8.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}