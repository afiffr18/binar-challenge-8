package id.afif.binarchallenge8.presentation.ui

import androidx.compose.runtime.MutableState
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.afif.binarchallenge8.domain.model.MoviesDetail
import id.afif.binarchallenge8.repository.MoviesDetailRepo
import kotlinx.coroutines.launch

class MoviesDetailViewModel(private val repo: MoviesDetailRepo) : ViewModel() {

    lateinit var dataMoviesDetail: MutableState<MoviesDetail>

    private fun getMoviesDetail(movieId: Int) {
        viewModelScope.launch {
            val result = repo.getMoviesDetail(movieId)
            dataMoviesDetail.value = result
        }
    }
}