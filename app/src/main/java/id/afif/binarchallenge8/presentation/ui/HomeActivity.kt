package id.afif.binarchallenge8.presentation.ui

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.blur
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import id.afif.binarchallenge8.R
import id.afif.binarchallenge8.domain.model.Result
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeActivity : AppCompatActivity() {

    private val viewModel: MoviesViewModel by viewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val hitam = colorResource(id = R.color.black)
            val darkBlue = colorResource(id = R.color.dark_blue)

            val gradientBlue = Brush.verticalGradient(0f to darkBlue, 1000f to hitam)

            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.TopCenter
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(gradientBlue)
                        .blur(16.dp)
                )
                LayoutCard()
            }

        }
    }

    @Suppress("OPT_IN_IS_NOT_ENABLED")
    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    fun LayoutCard() {
        val results = viewModel.dataMovies.value
        LazyVerticalGrid(
            cells = GridCells.Fixed(2),
            contentPadding = PaddingValues(8.dp)
        ) {
            itemsIndexed(
                items = results
            ) { index, item ->
                CardMovies(result = item, onClick = {})
            }
        }
    }

    @Composable
    fun CardMovies(
        result: Result,
        onClick: () -> Unit
    ) {

        Card(
            shape = MaterialTheme.shapes.medium,
            modifier = Modifier
                .padding(10.dp)
                .fillMaxWidth()
                .heightIn(220.dp)
                .clickable(onClick = onClick),
            elevation = 8.dp,
            backgroundColor = colorResource(id = R.color.little_dark_blue)
        ) {
            Column {

                AsyncImage(
                    model = ImageRequest.Builder(LocalContext.current)
                        .data("https://www.themoviedb.org/t/p/w220_and_h330_face/${result.posterPath}")
                        .build(),
                    contentDescription = "barcode image",
                    modifier = Modifier
                        .fillMaxWidth()
                        .heightIn(200.dp),
                    contentScale = ContentScale.FillBounds
                )

                Spacer(modifier = Modifier.padding(3.dp))
                Text(
                    text = result.title,
                    modifier = Modifier.padding(start = 5.dp, top = 5.dp),
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    fontWeight = FontWeight.Bold,
                    color = Color.LightGray
                )
                Text(
                    text = result.releaseDate,
                    fontSize = 10.sp,
                    modifier = Modifier.padding(start = 5.dp, bottom = 2.dp),
                    color = Color.Gray
                )


            }
        }
    }


}