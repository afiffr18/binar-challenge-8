package id.afif.binarchallenge8.presentation.ui

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.afif.binarchallenge8.domain.model.Result
import id.afif.binarchallenge8.repository.PopularMoviesRepoImpl
import kotlinx.coroutines.launch
import java.io.IOException


class MoviesViewModel(private val moviesRepo: PopularMoviesRepoImpl) : ViewModel() {

    val dataMovies: MutableState<List<Result>> = mutableStateOf(emptyList())

    init {
        getPopularMovies()
    }


    private fun getPopularMovies() {
        viewModelScope.launch {
            try {
                val result = moviesRepo.getPopularMovies("c548b9c05e09ed4c22de8c8eed87a602", 1)
                dataMovies.value = result
            } catch (ex: IOException) {
                Log.e("ErrorGet", ex.message.toString())
            }

        }
    }


}