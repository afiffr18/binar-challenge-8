package id.afif.binarchallenge8.presentation.ui

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.blur
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import id.afif.binarchallenge8.R

class RegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val hitam = colorResource(id = R.color.black)
            val darkBlue = colorResource(id = R.color.dark_blue)

            val gradientBlue = Brush.verticalGradient(0f to darkBlue, 1000f to hitam)

            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.TopCenter
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(gradientBlue)
                        .blur(16.dp)
                )
                LayoutInit()
            }
        }
    }

    @Composable
    @Preview(showSystemUi = true)
    fun LayoutInit() {
        var username by remember { mutableStateOf("") }
        var email by remember { mutableStateOf("") }
        var password by remember { mutableStateOf("") }
        var confirmPassword by remember { mutableStateOf("") }
        Column(
            modifier = Modifier
                .padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "REGISTER",
                fontSize = 36.sp,
                fontWeight = FontWeight.Bold,
                color = Color.White,
                textAlign = TextAlign.Center,
                modifier = Modifier.padding(top = 36.dp)
            )

            Spacer(modifier = Modifier.padding(15.dp))
            CustomTextField(
                text = username,
                onValueChange = {
                    username = it
                },
                label = "Username",
                visualTransformation = VisualTransformation.None
            )

            CustomTextField(
                text = email,
                onValueChange = {
                    email = it
                },
                label = "Email",
                visualTransformation = VisualTransformation.None
            )

            CustomTextField(
                text = confirmPassword,
                onValueChange = {
                    confirmPassword = it
                },
                label = "Password",
                visualTransformation = PasswordVisualTransformation()
            )
            CustomTextField(
                text = password,
                onValueChange = {
                    password = it
                },
                label = "Confirm Password",
                visualTransformation = PasswordVisualTransformation()
            )
            Spacer(modifier = Modifier.padding(8.dp))
            Button(
                onClick = { /*TODO*/ },
                modifier = Modifier
                    .fillMaxWidth(0.8f)
                    .height(40.dp),
                shape = RoundedCornerShape(5.dp),
                colors = ButtonDefaults.buttonColors(backgroundColor = colorResource(id = R.color.dark_blue))
            ) {
                Text(text = "REGISTER", color = Color.White)
            }
        }

    }


    @Composable
    fun CustomTextField(
        text: String,
        rightIcon: @Composable (() -> Unit)? = null,
        onValueChange: (String) -> Unit,
        label: String,
        visualTransformation: VisualTransformation
    ) {
        OutlinedTextField(
            value = text,
            modifier = Modifier.fillMaxWidth(0.8f),
            onValueChange = onValueChange,
            shape = RoundedCornerShape(10.dp),
            trailingIcon = rightIcon,
            label = { Text(text = label, color = Color.Gray) },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = Color.LightGray,
                unfocusedBorderColor = Color.Gray,
                textColor = Color.White, cursorColor = colorResource(id = R.color.light_blue)
            ),
            visualTransformation = visualTransformation
        )
    }
}