package id.afif.binarchallenge8.repository

import id.afif.binarchallenge8.domain.model.Result

interface PopularMoviesRepo {

    suspend fun getPopularMovies(apiKey: String, page: Int): List<Result>
}