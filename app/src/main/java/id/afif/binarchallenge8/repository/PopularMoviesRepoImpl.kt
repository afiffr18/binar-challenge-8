package id.afif.binarchallenge8.repository

import id.afif.binarchallenge8.domain.model.Result
import id.afif.binarchallenge8.network.ApiService
import id.afif.binarchallenge8.network.model.ResultDtoMapper

class PopularMoviesRepoImpl(
    private val apiService: ApiService,
    private val mapper: ResultDtoMapper
) : PopularMoviesRepo {

    override suspend fun getPopularMovies(apiKey: String, page: Int): List<Result> {
        val result = apiService.getPopularMovies(apiKey, page).results
        return mapper.toDomainList(result)
    }
}