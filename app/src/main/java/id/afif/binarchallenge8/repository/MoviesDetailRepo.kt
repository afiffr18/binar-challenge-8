package id.afif.binarchallenge8.repository

import id.afif.binarchallenge8.domain.model.MoviesDetail
import id.afif.binarchallenge8.network.ApiService
import id.afif.binarchallenge8.network.model.moviesdetail.MoviesDetailDtoMapper

class MoviesDetailRepo(
    private val apiService: ApiService,
    private val mapper: MoviesDetailDtoMapper
) {

    suspend fun getMoviesDetail(movieId: Int): MoviesDetail {
        val result = apiService.getMovieDetail("", movieId)
        return mapper.mapToDomainModel(result)
    }
}