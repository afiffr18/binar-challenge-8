package id.afif.binarchallenge8.domain.model


import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class PopularMovies(
    val page: Int,
    val results: List<Result>,
    val totalPages: Int,
    val totalResults: Int
) : Parcelable