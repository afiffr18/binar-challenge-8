package id.afif.binarchallenge8.domain.model

import id.afif.binarchallenge8.network.model.moviesdetail.Genre

data class MoviesDetail(
    val id: Int,
    val genres: List<Genre>,
    val overview: String,
    val posterPath: String,
    val releaseDate: String,
    val runtime: Int,
    val tagline: String,
    val title: String,
    val voteAverage: Double,

    )