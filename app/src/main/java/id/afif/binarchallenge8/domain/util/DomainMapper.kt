package id.afif.binarchallenge8.domain.util

interface DomainMapper<T, DomainModel> {

    fun mapToDomainModel(modelDto: T): DomainModel
}
