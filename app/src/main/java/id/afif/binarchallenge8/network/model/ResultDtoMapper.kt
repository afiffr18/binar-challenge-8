package id.afif.binarchallenge8.network.model

import id.afif.binarchallenge8.domain.model.Result
import id.afif.binarchallenge8.domain.util.DomainMapper

class ResultDtoMapper : DomainMapper<ResultDto, Result> {
    override fun mapToDomainModel(modelDto: ResultDto): Result {
        return Result(
            adult = modelDto.adult,
            backdropPath = modelDto.backdropPath,
            genreIds = modelDto.genreIds,
            id = modelDto.id,
            originalLanguage = modelDto.originalLanguage,
            originalTitle = modelDto.originalTitle,
            overview = modelDto.overview,
            popularity = modelDto.popularity,
            posterPath = modelDto.posterPath,
            releaseDate = modelDto.releaseDate,
            title = modelDto.title,
            video = modelDto.video,
            voteAverage = modelDto.voteAverage,
            voteCount = modelDto.voteCount
        )
    }


    fun toDomainList(initial: List<ResultDto>): List<Result> {
        return initial.map {
            mapToDomainModel(it)
        }
    }

}