package id.afif.binarchallenge8.network.model.moviesdetail

import id.afif.binarchallenge8.domain.model.MoviesDetail
import id.afif.binarchallenge8.domain.util.DomainMapper

class MoviesDetailDtoMapper : DomainMapper<MoviesDetailDto, MoviesDetail> {
    override fun mapToDomainModel(modelDto: MoviesDetailDto): MoviesDetail {
        return MoviesDetail(
            id = modelDto.id,
            genres = modelDto.genres,
            overview = modelDto.overview,
            posterPath = modelDto.posterPath,
            releaseDate = modelDto.releaseDate,
            runtime = modelDto.runtime,
            tagline = modelDto.tagline,
            title = modelDto.title,
            voteAverage = modelDto.voteAverage
        )
    }

    fun toDomainList(modelDto: List<MoviesDetailDto>): List<MoviesDetail> {
        return modelDto.map {
            mapToDomainModel(it)
        }
    }
}