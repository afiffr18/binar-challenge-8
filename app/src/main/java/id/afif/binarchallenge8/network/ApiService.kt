package id.afif.binarchallenge8.network

import id.afif.binarchallenge8.network.model.moviesdetail.MoviesDetailDto
import id.afif.binarchallenge8.network.response.PopularMoviesResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("movie/popular")
    suspend fun getPopularMovies(
        @Query("api_key") key: String,
        @Query("page") page: Int
    ): PopularMoviesResponse

    @GET("movie/{movie_id}")
    suspend fun getMovieDetail(
        @Query("api_key") key: String,
        @Path("movie_id") movieId: Int
    ): MoviesDetailDto
}