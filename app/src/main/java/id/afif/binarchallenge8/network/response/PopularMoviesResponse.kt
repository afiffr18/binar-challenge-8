package id.afif.binarchallenge8.network.response


import com.google.gson.annotations.SerializedName
import id.afif.binarchallenge8.network.model.ResultDto

data class PopularMoviesResponse(
    @SerializedName("page")
    val page: Int,
    @SerializedName("results")
    val results: List<ResultDto>,
    @SerializedName("total_pages")
    val totalPages: Int,
    @SerializedName("total_results")
    val totalResults: Int
)